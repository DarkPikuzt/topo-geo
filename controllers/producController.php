<?php
require_once '../models/product.php';

$product = new product();


//regtificamos las opciones
$registra = (isset($_POST['agregar'])) ? $_POST['agregar'] : '0';
$editar = (isset($_POST['editar'])) ? $_POST['editar'] : '0';
$eliminar = (isset($_POST['eliminar'])) ? $_POST['eliminar'] : '0';

//var_dump('editar',$editar);
//var_dump('registra:',$registra);
//var_dump('eliminar:',$eliminar);

if(($registra == 1)&&( $editar == 0)&&($eliminar==0)){
    $op = 'registrar';
}elseif (($registra == 0)&&( $editar == 1)&&($eliminar==0)){
    $op = 'editar';
}elseif (($registra == 0)&&( $editar == 0)&&($eliminar== 1)){
    $op = 'eliminar';
}
//var_dump($op);


switch ($op){
    case 'registrar':


       //echo'registrar';
        //var_dump($data);
        $product->setTitle($_POST['titulo']);
        $product->setPrice($_POST['precio']);
        $product->setDescription($_POST['descripcion']);
        $product->setIdCategory($_POST['categoria']);
        $product->setIdbrand($_POST['marca']);
        $product->setNameImg($_FILES['img']['name']);
        $product->setTypeImg($_FILES['img']['type']);
        $product->setSizeImg($_FILES['img']['size']);
        $product->setTempdestino($_FILES['img']['tmp_name']);
        $product->setDestination($_SERVER['DOCUMENT_ROOT'].'/assets/principal/images/topo-geo/productos/');
        $product->setDestinationPdf($_SERVER['DOCUMENT_ROOT'].'/assets/principal/pdf/');
        $product->setPdf($_FILES['pdf']['name']);
        $product->setTempPdf($_FILES['pdf']['tmp_name']);
        $product->setTypePdf($_FILES['pdf']['type']);

        /*creamos un  nuevo nombre para la imagen*/
        $date = date("dHis");
        $nRandom = rand(10,99);
        $newNameNombreArchive ="imagen_".$date.$nRandom.".".pathinfo($_FILES['img']['name'],PATHINFO_EXTENSION);
        $newRoute = $product->getDestination().$newNameNombreArchive;
        //var_dump($newRoute);
        $product->setNewNameImg($newNameNombreArchive);


        $data = array(
            'agregar'=>$registra,
            'title' => $product->getTitle(),
            'price' => $product->getPrice(),
            'description' => $product->getDescription(),
            'nameImg'=> $product->getNameImg(),
            'type' => $product->getTypeImg(),
            'size'=>$product->getSizeImg(),
            'newNameImg'=>$product->getNewNameImg(),
            'temp'=> $product->getTempdestino(),
            'newRoute'=>$newRoute,
            'category' => $product->getIdCategory(),
            'namePdf'=> $product->getPdf(),
            'tempPdf'=> $product->getTempPdf(),
            'newRoutePdf' => $product->getDestinationPdf().$product->getPdf(),
            'typePdf'=> $product->getTypePdf()
        );

        //var_dump($data);

        if ($data['typePdf'] == null){
            if (($data['type'] == 'image/jpeg') ||($data['type'] == 'image/png') ||($data['type'] == 'image/jpg')){
                move_uploaded_file($data['temp'],$data['newRoute']);
                $addProduct = $product->addProducto();
                echo $addProduct;
                //echo'Almacenado';

            }else{
                echo 'Solo se almiten archivos JPG, JPEG, PNG';
            }


        } elseif($data['size'] <= 1000000){
            if (($data['type'] == 'image/jpeg') ||($data['type'] == 'image/png') ||($data['type'] == 'image/jpg')){
                if ($data['typePdf'] == 'application/pdf'){
                    move_uploaded_file($data['temp'],$data['newRoute']);
                    move_uploaded_file($data['tempPdf'],$data['newRoutePdf']);
                    $addProduct = $product->addProducto();
                    echo $addProduct;
                }else{
                    echo'Solo se almiten Archivos formato pdf';
                }
            }else{
                echo 'Solo se almiten archivos JPG, JPEG, PNG';
            }
        }else{
            echo 'No almiten imagenes mayores a 1 MG o no se agrego un archivo';
        }


        break;
    case'editar':
        $product->setTitle($_POST['tituloU']);
        $product->setPrice($_POST['precioU']);
        $product->setDescription($_POST['descripcionU']);
        $product->setIdCategory($_POST['categoriaU']);
        $product->setIdbrand($_POST['marcaU']);
        $product->setId($_POST['id']);
        $product->setNameImg($_FILES['imgU']['name']);
        $product->setTypeImg($_FILES['imgU']['type']);
        $product->setSizeImg($_FILES['imgU']['size']);
        $product->setTempdestino($_FILES['imgU']['tmp_name']);
        $product->setDestination($_SERVER['DOCUMENT_ROOT'].'/assets/principal/images/topo-geo/productos/');
        $product->setDestinationPdf($_SERVER['DOCUMENT_ROOT'].'/assets/principal/pdf/');
        $product->setPdf($_FILES['pdfU']['name']);
        $product->setTempPdf($_FILES['pdfU']['tmp_name']);
        $product->setTypePdf($_FILES['pdfU']['type']);

        /*creamos un  nuevo nombre para la imagen*/

        $date = date("dHis");
        $nRandom = rand(10,99);
        $newNameNombreArchive ="imagen_".$date.$nRandom.".".pathinfo($_FILES['imgU']['name'],PATHINFO_EXTENSION);
        $newRoute = $product->getDestination().$newNameNombreArchive;
        //var_dump($newRoute);
        $product->setNewNameImg($newNameNombreArchive);

        $data = array(
            'id'=>$product->getId(),
            'agregar'=>$registra,
            'title' => $product->getTitle(),
            'price' => $product->getPrice(),
            'description' => $product->getDescription(),
            'nameImg'=> $product->getNameImg(),
            'type' => $product->getTypeImg(),
            'size'=>$product->getSizeImg(),
            'newNameImg'=>$product->getNewNameImg(),
            'temp'=> $product->getTempdestino(),
            'newRoute'=>$newRoute,
            'category' => $product->getIdCategory(),
            'namePdf'=> $product->getPdf(),
            'tempPdf'=> $product->getTempPdf(),
            'newRoutePdf' => $product->getDestinationPdf().$product->getPdf(),
            'typePdf'=> $product->getTypePdf()

        );



        if(empty($data['nameImg']) && empty($data['namePdf'])){
            $op = 'Frm';
        }else if (!empty($data['nameImg']) && empty($data['namePdf'])){
            $op = 'img';
        }else if (empty($data['nameImg']) && !empty($data['namePdf'])){
            $op = 'pdf';
        }
        //var_dump($op);




        switch ($op){
            case 'Frm':

                $update = $product->updateProducto();
                echo $update;
                break;

            case'img':
                //echo 'img';
                if($data['size'] <= 1000000){
                    if (($data['type'] == 'image/jpeg') ||($data['type'] == 'image/png') ||($data['type'] == 'image/jpg')){
                        move_uploaded_file($data['temp'],$data['newRoute']);
                        $deleteFileImg =$product->deleteFileImg();
                        $updateImg = $product->updateImg();
                        $update = $product->updateProducto();
                        echo (($updateImg == true)&&($update == true))? 1 : 0;

                    }else{
                        echo 'Solo se almiten archivos JPG, JPEG, PNG';
                    }
                }else{
                    echo 'No almiten imagenes mayores a 1 MG o no se agrego un archivo';
                }
                break;
            case 'pdf':
                if ($data['typePdf'] == 'application/pdf'){
                    $deleteFilePdf = $product->deleteFilePdf();
                    move_uploaded_file($data['tempPdf'],$data['newRoutePdf']);
                    $updatePdf = $product->updatePdf();
                    $update= $product->updateProducto();
                    echo (($updatePdf == true)&&($update == true))? 1 : 0;
                }else{
                    echo'Solo se almiten Archivos formato pdf';
                }
                break;
        }
        break;
    case 'eliminar':
        //echo 'dentro de eliminar';
        $product->setId($_POST['id']);
        $product->setDestination($_SERVER['DOCUMENT_ROOT'].'/assets/principal/images/topo-geo/productos/');
        $deleteImg = $product->deleteFileImg();
        $delete = $product->deleteProduct();
        echo $delete;

        break;
}














