<?php

require_once '../models/carrusel.php';
$carrusel = new carrusel();

/*pedimos variables*/
$carrusel->setDescrition($_POST['descripcion']);
$carrusel->setIdImg($_POST['id']);
$carrusel->setNameImg($_FILES['img']['name']);
$carrusel->setTypeImg($_FILES['img']['type']);
$carrusel->setSizeImg($_FILES['img']['size']);
$carrusel->setTempdestino($_FILES['img']['tmp_name']);
$carrusel->setDestination($_SERVER['DOCUMENT_ROOT'].'/assets/principal/images/topo-geo/carrusel/');

/*creamos un  nuevo nombre para la imagen*/
$date = date("dHis");
$nRandom = rand(10,99);
$newNameNombreArchive ="imagen_".$date.$nRandom.".".pathinfo($_FILES['img']['name'],PATHINFO_EXTENSION);
$newRoute = $carrusel->getDestination().$newNameNombreArchive;
//var_dump($newNameNombreArchive);
$carrusel->setNewNameImg($newNameNombreArchive);

$data = array(
    'route'=> $carrusel->getDestination(),
    'name'=> $carrusel->getNameImg(),
    'size'=> $carrusel->getSizeImg(),
    'type'=> $carrusel->getTypeImg(),
    'temp'=> $carrusel->getTempdestino(),
    'description' => $carrusel->getDescrition(),
    'newRoute' =>$newRoute,
    'newNameImg' =>$carrusel->getNewNameImg()
);
if(empty($data['name'])){
    $op = 'descrition';
}else{
    $op = 'img';
}

//var_dump($data);
switch ($op){
    case 'descrition':
        $update = $carrusel->UpdateCarrusel();
        echo $update;
        break;
    case'img':

        if($data['size'] <= 1000000){
            if (($data['type'] == 'image/jpeg') ||($data['type'] == 'image/png') ||($data['type'] == 'image/jpg')){
                move_uploaded_file($data['temp'],$data['newRoute']);
                $updateDescription = $carrusel->UpdateCarrusel();
                $deleteFileImg = $carrusel->deleteFileImg();
                $updateImg = $carrusel->updateImage();
                echo ($updateDescription === $updateImg)? 1: 'No se almaceno';
            }else{
                echo 'Solo se almiten archivos JPG, JPEG, PNG';
            }
        }else{
            echo 'No almiten imagenes mayores a 1 MG o no se agrego un archivo';
        }
        break;

}