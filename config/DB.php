<?php
/**
 * Created by PhpStorm.
 * User: Pikuzt
 * Date: 22/3/2019
 * Time: 11:46
 */

class DB extends PDO
{

    private $host ='localhost';//	hv34svg132
    private $database = 'dbtopogeo';//topgeoap_DB
    private $user = 'root';//topgeoap_root
    private $password = '';//TopoGeo_123456@
    private $pdo;
    private $query;
    private $bConnected = false;
    private $parameters;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $dns = 'mysql:dbname='.$this->database.';host='.$this->host;
       parent::__construct($dns, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));}

    public  function connection (){
        $dns = 'mysql:dbname='.$this->database.';host='.$this->host;
        try{

            $this -> pdo = new PDO($dns, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this -> pdo -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);


            return $this->pdo;

        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }

    }

    public function closeConnecion(){
        $this->pdo = null;
    }


}