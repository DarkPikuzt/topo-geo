<footer class="site-footer fuente">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
	            <div>
                    <a href="https://www.facebook.com/topgeoap/" class="pl-0 pr-3" target="_blank"><span class="icon-facebook"></span></a>
                    <a href="#" class="pl-3 pr-3" target="_blank"><span class="icon-twitter"></span></a>
                    <a href="https://www.instagram.com/topgeoap_/" class="pl-3 pr-3" target="_blank"><span class="icon-instagram"></span></a>

                </div>
            </div>
            <div class="col-md-12 text-center">
                <div>
                    <p><a href="#">Aviso de Privacidad</a></p>
                </div>
            </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
            <div class="col-md-12">
                <p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved |<a href="http://ctconsulting.mx/" target="_blank" >CTCONSUNTING</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
    </div>
</footer>