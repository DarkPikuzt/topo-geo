
<style>
    .img-border{
        border-radius: 8px;
    }



</style>
<!--Sobre nosotros-->

<section class="site-section bg-white fuente">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12 col-lg-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item menu-hover">
                        <button class="nav-link bg-alice active" id="home-tab" data-toggle="tab" href="#home-proy" role="tab" aria-controls="home" aria-selected="true"><?php echo $title[1]?></button>
                    </li>
                    <li class="nav-item menu-hover">
                        <button class="nav-link bg-alice " id="profile-tab" data-toggle="tab" href="#profile-proy" role="tab" aria-controls="profile" aria-selected="false"><?php echo $title[2]?></button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link menu-hover bg-alice " id="contact-tab" data-toggle="tab" href="#contact-proy" role="tab" aria-controls="contact" aria-selected="false"><?php echo $title[3]?></button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home-proy" role="tabpanel" aria-labelledby="home-tab">
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-lg-6  text-center   ">
                                <img src="assets/principal/images/topo-geo/carrusel/<?php echo $img[1]?>"
                                     class="img-fluid img-border">
                            </div>
                            <div class="col-md-12 col-lg-6 ">
                                <div class="site-section-title overlay">
                                    <h2 class="title-h2 h5 text-black mb-3 text_font-h2 text-center"><?php echo $location[1]?></h2>
                                </div>
                                <p class="text_font text-justify">
                                    <?php echo $description[1]?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile-proy" role="tabpanel" aria-labelledby="profile-tab">
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-lg-6  text-center   ">
                                <img src="assets/principal/images/topo-geo/carrusel/<?php echo $img[2]?>"
                                     class="img-fluid img-border">
                            </div>
                            <div class="col-md-12 col-lg-6">
                                <div class="site-section-title overlay">
                                    <h2 class="title-h2 text-center"><?php echo $location[2]?></h2>
                                </div>
                                <p class="text_font text-justify">
                                    <?php echo $description[2]?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact-proy" role="tabpanel" aria-labelledby="contact-tab">
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-lg-6  text-center   ">
                                <img src="assets/principal/images/topo-geo/carrusel/<?php echo $img[3]?>"
                                     class="img-fluid img-border">
                            </div>
                            <div class="col-md-12 col-lg-6">
                                <div class="site-section-title overlay">
                                    <h2 class="title-h2 text-center"><?php echo $location[3]?></h2>
                                </div>
                                <p class="text_font text-justify">
                                    <?php echo $description[3]?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>












<script>

    $('#myTab a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
