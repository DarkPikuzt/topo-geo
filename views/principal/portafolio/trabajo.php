
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();

$sql="SELECT * FROM portfolio";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();
?>

<style>
    .bg-alice{
        background: aliceblue;
    }
</style>
<!--Portafolio-->
<div class="site-section bg-alice fuente" id="portafolio">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-12 text-center">
                <div class="site-section-title">
                    <h2>Casos de exito</h2>
                </div>
                <p>En la creación de nuestra historia hemos participado con mucho éxito en diferentes proyectos en toda la república Mexicana, y gracias a la preferencia de todos ustedes es lo que nos motiva a continuar escribiendo nuestra historia pero también apoyando a escribir las historias de nuestros asociados presentes y futuros.</p>
            </div>
        </div>
        <div class="row">
            <?php foreach ($result  as $row):?>
                <div class="col-md-12 col-lg-4 mb-12 text-center" data-aos="fade-up" data-aos-delay="100">
                    <div class="p-4 bg-white">
                        <div class="row text-center">
                            <div class="col-md-12 col-lg-12 mb-6">
                                <h2 class="h5 text-black mb-3"><?php echo $row['title'] ?></h2>
                            </div>
                            <div class="col-md-12 col-lg-12 mb-6">
                                <span class="d-block text-secondary small text-uppercase"><?php echo $row['location'] ?></span>
                            </div>
                            <div class="col-md-12 col-lg-12 mb-12">
                               <img src="assets/principal/images/topo-geo/carrusel/<?php echo $row['img'] ?>" alt="Image" class="img-fluid">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>
                        <p class="text-justify"><?php echo $row['description'] ?></p>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>