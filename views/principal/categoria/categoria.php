
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();

$sql="SELECT * FROM category";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();

$sqlB="SELECT * FROM brand";
$queryB = $conn->prepare($sqlB);
$queryB->execute();
$resultB = $queryB->fetchAll();

$sqlAv="SELECT * FROM note";
$queryAv = $conn->prepare($sqlAv);
$queryAv->execute();
$resultAv = $queryAv->fetchAll();


?>

<style>
    .bg-alice{
        background: aliceblue;
    }

</style>
<!--Categorias-->
<div class="site-section site-section-sm pb-0 bg-alice fuente">
	<div class="container">
        <div class="row ">
            <div class="form-search col-md-12" style="margin-top: 65px;">

                <form action=""  id="frmBuscar">
                    <div class="row align-items-end">
                        <div class="col-md-3">
                            <label for="offer-types">Marca</label>
                            <div class="select-wrap">
                                <span class="icon icon-arrow_drop_down"></span>
                                <select  id="marca" name="marca" class="form-control d-block rounded-0">
                                    <option value="" selected>Seleccione...</option>
                                    <?php foreach ($resultB  as $rowB):?>
                                        <option value="<?php echo $rowB['id_brand']?>"><?php echo $rowB['name_brand']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="offer-types">Categoria</label>
                            <div class="select-wrap">
                                <span class="icon icon-arrow_drop_down"></span>
                                <select  id="producto" name="producto" class="form-control d-block rounded-0">
                                    <option value="" selected>Seleccione...</option>
                                    <?php foreach ($result  as $row):?>
                                        <option value="<?php echo $row['id_category']?>"><?php echo $row['name_category']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label for="offer-types">Buscar</label>
                            <input type="text" id="myInput"  name="search" class="form-control" placeholder="buscar" aria-label="Recipient's username" aria-describedby="basic-addon2">

                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-success text-white btn-block rounded-0" id="btnBuscarCategoria" >Buscar </button>
                        </div>
                    </div>
                </form>


                <!--Tipo de vista  catalogo-->
                <?php foreach ($resultAv  as $rowAv):?>
                    <div class="col-md-12 text-center">
                        <?php if (!empty($rowAv['note'])): ?>
                            <div class="form-group text-center" style="color: white">
                                <strong >Avisos:</strong> <?php echo $rowAv['note']?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>

	
	</div>
</div><!--/categorias-->







   <div id="resultado" >

   </div>






