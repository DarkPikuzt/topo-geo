<!--Nuestros Clientes-->
<style>
    .bg-alice{
        background: aliceblue;
    }
</style>

<div class="site-section bg-alice fuente" id="contacto">
    <br>
    <br>
    <br>
    <div class="container">
        <div class="row mb-5 justify-content-center">
            <div class="col-md-7">
                <div class="site-section-title text-center">
                    <h2>Contacto</h2>
                </div>
            </div>
        </div>
        <div class="row">
	        <div class="col-md-6 col-lg-4 mb-5 mb-lg-5">
		        <div class="team-member">
			        <div class="text-center">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3791.48249115947!2d-94.4364557858016!3d18.141674285388444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85e982ff3a9c909b%3A0x8c843536ac27d4bb!2sTopografia+Y+Geodecia+Aplicada!5e0!3m2!1ses!2smx!4v1555107530947!5m2!1ses!2smx" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
			       <div class="text">
				        <h2 class="mb-2 font-weight-light text-black h4 text-center">Oficina Coatzacoalcos</h2>
                       <h5 class="text-center"> Tel: 921 2141557</h5>
                   </div>
		        </div>
	        </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-5">
                <div class="team-member">
                    <div class="text-center">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3760.1395098883513!2d-96.90831394211891!3d19.53562319515839!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db31f499555555%3A0x79afe45d51d5f1ac!2sTopografia+y+Geodesia+Aplicada!5e0!3m2!1ses!2smx!4v1555107301982!5m2!1ses!2smx" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                    <div class="text">
                        <h2 class="mb-2 font-weight-light text-black h4 text-center">Matriz Xalapa </h2>
                        <h5 class="text-center">Tel: 01-228-8904057</h5>
                        <h5 class="text-center">Cel: 2281691100</h5>
                    </div>
                </div>
            </div>
	        <div class="col-md-6 col-lg-4 mb-5 mb-lg-5">
		        <div class="team-member">
			        <div class="text-center">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3794.867236503929!2d-92.95173067339378!3d17.984907563746386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85edd79b859e2289%3A0x753ca3779b21dfe2!2sAv.+27+de+Febrero+2406%2C+Cuadrante+II%2C+Atasta+de+Serra%2C+86100+Villahermosa%2C+Tab.!5e0!3m2!1ses!2smx!4v1555107010809!5m2!1ses!2smx" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div>
			        <div class="text">
				        <h2 class="mb-2 font-weight-light text-black h4 text-center">Oficina Villahermosa</h2>
                        <h5 class="text-center">Cel: 2281691100</h5>
			        </div>
		        </div>
	        </div>
        </div>



        <div class="row mb-5 justify-content-center">
            <div class="col-md-7">
                <div class="site-section-title text-center">
                    <h2>Sucursales</h2>
                </div>
            </div>
        </div>

        <?php
        require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';


        $conexion = new DB();
        $conn =$conexion->connection();
        $sql=" select * from branch_office";
        $query = $conn->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();





        ?>









        <style>
            .whatsapp {
                position:fixed;
                width:60px;
                height:60px;
                bottom:40px;
                right:40px;
                background-color:#25d366;
                color:#FFF;
                border-radius:50px;
                text-align:center;
                font-size:30px;
                z-index:100;
            }

            .whatsapp-icon {
                margin-top:13px;
            }

            .google-maps {
                position: relative;
                padding-bottom: 75%;
                height: 0;
                overflow: hidden;
            }
            .google-maps iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100% !important;
                height: 100% !important;
            }
        </style>
        <a href="https://api.whatsapp.com/send?phone=2281691100" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>



        <div class="row">


        <?php foreach ($result as $item): ?>

                <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                    <div class="team-member ">
                        <div class="text-center google-maps">
                            <?php echo $item['frame_office']?>
                        </div>
                        <div class="text">
                            <h2 class="mb-2 font-weight-light text-black h4 text-center"><?php echo $item['name_office']?></h2>
                            <h5 class="text-center"> calle: <?php echo $item['street_office']?></h5>
                            <h5 class="text-center"> Colonia: <?php echo $item['colony_office']?></h5>
                            <h5 class="text-center"> Codigo Postal: <?php echo $item['cp_office']?></h5>
                        </div>
                    </div>
                </div>

        <?php endforeach;?>
    </div>








        <div class="container">
            <div class="row">
                <div class="col-md-12 p-2">
                    <form id="frmEmail" class="p-5 bg-white border">
                        <div class="row form-group" id="valNombreEmail">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="fullname">Nombre</label>
                                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre Compreto">
                            </div>
                        </div>
                        <div class="row form-group"  id="valEmail">
                            <div class="col-md-12">
                                <label class="font-weight-bold" for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control" placeholder="Email ">
                            </div>
                        </div>
                        <div class="row form-group" id="valAsunto">
                            <div class="col-md-12">
                                <label class="font-weight-bold" for="email">Asunto</label>
                                <input type="text" id="asunto" name="asunto" class="form-control" placeholder="Asunto">
                            </div>
                        </div>
                        <div class="row form-group" id="valMensaje">
                            <div class="col-md-12">
                                <label class="font-weight-bold" for="message">Mensaje</label>
                                <textarea  id="mensaje" name="mensaje" cols="30" rows="5" class="form-control" placeholder="Escriba su mensaje"></textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="button" value="Enviar" id="btnEmail" class="btn btn-primary  py-2 px-4 rounded-0">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
</div>



<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v9.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/es_ES/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="502755429904445"
     theme_color="#0A7CFF"
     logged_in_greeting=" ¡Hola! como podemos ayudarte?"
     logged_out_greeting=" ¡Hola! como podemos ayudarte?">
</div>