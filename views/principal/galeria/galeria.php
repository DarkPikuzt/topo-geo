
<?php
// var_dump($_SERVER['DOCUMENT_ROOT'].'/config/DB.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';



$conexion = new DB();
$conn =$conexion->connection();

$sql="SELECT * FROM gallery";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();


?>

<style>
    .site-section{
        padding: 2em 0 !important;
    }

</style>


<!--Galeria "carrusel"-->
<div class="slide-one-item home-slider owl-carousel fuente">
    <?php foreach ($result  as $row):?>
	<!--Primer  Imagen-->
	<div class="site-blocks-cover overlay" style="background-image: url(assets/principal/images/topo-geo/carrusel/<?php echo $row['nameImg'] ?>);" data-aos="fade" data-stellar-background-ratio="0.5">
		<div class="container">
			<div class="row align-items-center justify-content-center text-center">
				<div class="col-md-10">
					<h1 class="mb-2"><?php echo $row['description']?></h1>
				</div>
			</div>
		</div>
	</div>
    <?php endforeach;?>

</div><!--Galeria "carrusel"-->