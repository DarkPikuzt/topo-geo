<style>

    .row .color h2 {
        color: whitesmoke;
    }

    .text_font{
        font-size: 3vh !important;
    }

    .title-h2{
        background:#c4ffc6 ;
        color: #8e8b8b !important;
        font-size: 15px;
        border-radius: 8px;
        text-transform: none !important;
    }
</style>


<!--Sobre nosotros-->
<div class="site-section bg-white fuente" id="nosotros" >
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center   ">
                <img src="assets/principal/images/topo-geo/carrusel/imagen_1015520139.jpg"
                class="img-fluid">
            </div>
            <div class="col-md-6 text-center   ">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="site-section-title overlay">
                                <h2 class="title-h2 h5 text-black mb-3 text_font-h2">Nosotros</h2>
                            </div>
                            <p class="text_font text-justify">
                                Somos una empresa fundada en el año 2003 en la ciudad de Xalapa, ver., ofreciendo servicios de venta, asesoría y capacitación de equipo topográfico y geodésico de las marcas sokkia, topcon, spectra precisión, Nikon, Geosurv, Garmin, Gowin., servicios topográficos y geodésicos como: medición de terrenos, medición de ejidos para regularización ante el ran, elaboración de proyectos ejecutivos de caminos, redes de agua, redes de drenaje sanitario, realización de batimetrías, deslindes y control topográfico de obras civiles.
                            </p>
                        </div>
                        <div class="carousel-item">

                            <div class="site-section-title overlay">
                                <h2 class="title-h2">Visión</h2>
                            </div>
                            <p class="text_font text-justify">
                                Ser reconocidos como la mejor empresa de ingeniería, construcción, supervisión, gerencia de proyectos, venta de equipo geodésico topográfico (gps de precisión, rtk en una frecuencia o doble frecuencia, navegadores mapa móvil. etc.), estaciones totales marca (sokkia, topcon, leica.etc.) y distribuidores de software topográfico (civil cad). por su alto estándar de calidad, seguridad, respeto. cumpliendo total y puntualmente todos nuestros compromisos.
                            </p>

                        </div>
                        <div class="carousel-item">
                            <div class="site-section-title overlay">
                                <h2 class="title-h2">Valores</h2>
                            </div>
                            <p class="text_font text-justify">
                                Guía para el mejor funcionamiento de la empresa, y son:
                            <ul class="text-justify">
                                <li>Liderazgo: esforzarse en crecer como empresa y como personas.</li>
                                <li>Colaboración: potenciar el talento colectivo.</li>
                                <li>Calidad: búsqueda de la excelencia.</li>
                            </ul>
                            </p>
                        </div>
                        <div class="carousel-item">
                            <div class="site-section-title overlay">
                                <h2 class="title-h2">Misión</h2>
                            </div>
                            <p class="text_font text-justify">
                                La misión de nuestra empresa es ofertar nuestros servicios demostrando que se realizan con ética y profesionalismo, así como también ofrecer tecnología de vanguardia en el aspecto de equipos geodésicos y topográficos.
                            </p>


                        </div>
                    </div>

                </div>




           </div>

        </div>
    </div>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/views/principal/nosotros/galeria.php'?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/views/principal/nosotros/misiones.1.php'?>

