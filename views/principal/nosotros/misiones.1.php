
<style>
    .img-border{
        border-radius: 8px;
    }



</style>
<br>
<!--Sobre nosotros-->
<section class="site-section bg-white fuente" id="nosotros" >
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-6  text-center   ">
                    <img src="assets/principal/images/topo-geo/carrusel/imagen_1015520139.jpg"
                         class="img-fluid img-border">
                </div>
                <br>
                <div class="col-md-12 col-lg-6">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item menu-hover">
                            <button class="nav-link bg-alice active " id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Misión</button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link menu-hover bg-alice " id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Visión</button>
                        </li>
                        <li class="nav-item menu-hover ">
                            <button class="nav-link bg-alice " id="contact-tab" data-toggle="tab" href="#valores" role="tab" aria-controls="contact" aria-selected="false">Valores</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade  show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <br>
                            <div class="col-md-12">
                                <div class="site-section-title overlay">
                                    <h2 class="title-h2 text-center">Misión</h2>
                                </div>
                                <p class="text_font text-justify">
                                    La misión de nuestra empresa es ofertar nuestros servicios demostrando que se realizan con ética y profesionalismo, así como también ofrecer tecnología de vanguardia en el aspecto de equipos geodésicos y topográficos.
                                </p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <br>
                            <div class="col-md-12">
                                <div class="site-section-title overlay">
                                    <h2 class="title-h2 text-center">Visión</h2>
                                </div>
                                <p class="text_font text-justify">
                                    Ser reconocidos como la mejor empresa de ingeniería, construcción, supervisión, gerencia de proyectos, venta de equipo geodésico topográfico (gps de precisión, rtk en una frecuencia o doble frecuencia, navegadores mapa móvil. etc.), estaciones totales marca (sokkia, topcon, leica.etc.) y distribuidores de software topográfico (civil cad). por su alto estándar de calidad, seguridad, respeto. cumpliendo total y puntualmente todos nuestros compromisos.
                                </p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="valores" role="tabpanel" aria-labelledby="contact-tab">
                            <br>
                            <div class="col-md-12">
                                <div class="site-section-title overlay">
                                    <h2 class="title-h2 text-center">Valores</h2>
                                </div>
                                <p class="text_font text-justify">
                                    Guía para el mejor funcionamiento de la empresa, y son:
                                <ul class="text-justify">
                                    <li>Liderazgo: esforzarse en crecer como empresa y como personas.</li>
                                    <li>Colaboración: potenciar el talento colectivo.</li>
                                    <li>Calidad: búsqueda de la excelencia.</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</section>









<script>

    $('#myTab a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
