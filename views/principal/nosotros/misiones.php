<style>
    .bg-alice{
        background: aliceblue;
    }
</style>

<div class="site-section bg-alice  fuente">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4 mb-5 " data-aos="fade-up" data-aos-delay="200">
                <div class="p-4 bg-white">
                    <h2 class="h5 text-black mb-3 text-center title-h2"><strong>Visión</strong></h2>
                    <p class="text-justify">
                        Ser reconocidos como la mejor empresa de ingeniería, construcción, supervisión, gerencia de proyectos, venta de equipo geodésico topográfico (gps de precisión, rtk en una frecuencia o doble frecuencia, navegadores mapa móvil. etc.), estaciones totales marca (sokkia, topcon, leica.etc.) y distribuidores de software topográfico (civil cad). por su alto estándar de calidad, seguridad, respeto. cumpliendo total y puntualmente todos nuestros compromisos.
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 mb-5 " data-aos="fade-up" data-aos-delay="100">
                <div class="p-4 bg-white">
                    <h2 class="h5 text-black mb-3 text-center title-h2"><strong >Mision</strong></h2>
                    <p class="text-justify">
                        La misión de nuestra empresa es ofertar nuestros servicios demostrando que se realizan con ética y profesionalismo, así como también ofrecer tecnología de vanguardia en el aspecto de equipos geodésicos y topográficos.
                    </p>
                </div>
            </div>



            <div class="col-md-4 col-lg-4 mb-5" data-aos="fade-up" data-aos-delay="300">
                <div class="p-4 bg-white">
                    <h2 class="h5 text-black mb-3 text-center title-h2"><strong href="#">Valores</strong></h2>
                    <p class="text-justify">
                        Guía para el mejor funcionamiento de la empresa, y son:
                    <ul>
                         <li>Liderazgo: esforzarse en crecer como empresa y como personas.</li>
                         <li>Colaboración: potenciar el talento colectivo.</li>
                         <li>Calidad: búsqueda de la excelencia.</li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>
