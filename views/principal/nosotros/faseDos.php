<style>
    .pos{
        top:5%;
    }

    .row .color h2 {
        color: whitesmoke;
    }

    .text_font{
        font-size: 3vh !important;
    }

    .title-h2{
        background:#c4ffc6 ;
        color: #8e8b8b !important;
        font-size: 15px;
        border-radius: 8px;
        text-transform: none !important;
    }


</style>
<!--Sobre nosotros-->

<div class="site-section bg-alice fuente" id="nosotros">
    <div class="container">
        <br>
        <br>
        <br>
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <div class="site-section-title">
                    <h2>Sobre Nosotros</h2>
                </div>
                <p>
                    Somos una empresa fundada en el año 2003 en la ciudad de Xalapa, ver., ofreciendo servicios de venta, asesoría y capacitación de equipo topográfico y geodésico de las marcas sokkia, topcon, spectra precisión, Nikon, Geosurv, Garmin, Gowin., servicios topográficos y geodésicos como: medición de terrenos, medición de ejidos para regularización ante el ran, elaboración de proyectos ejecutivos de caminos, redes de agua, redes de drenaje sanitario, realización de batimetrías, deslindes y control topográfico de obras civiles.
                </p>
            </div>
        </div>
        <div class="row">
            <?php require_once 'misiones.1.php'?>
        </div>
    </div>
</div>







<?php require_once $_SERVER['DOCUMENT_ROOT'].'/views/principal/nosotros/galeria.php'?>

