<style>
    .bg-alice{
        background: aliceblue;
    }
</style>

<div class="site-section bg-alice  fuente">

    <div class="container">

        <div class="row">

            <div class="col-md-6 col-lg-4 mb-5" data-aos="fade-up" data-aos-delay="100">
                <a href="#"><img src="assets/principal/images/topo-geo/carrusel/eje1.jpg" alt="Image" class="img-fluid"></a>
                <div class="p-4 bg-white">
                    <h2 class="h5 text-black mb-3"><strong >Trabajo en Equipo</strong></h2>
                    <p class="text-justify">La suma de esfuerzos y la fluidez de las ideas permiten alcanzar el éxito.</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-5" data-aos="fade-up" data-aos-delay="200">
                <a href="#"><img src="assets/principal/images/topo-geo/carrusel/eje2.jpg" alt="Image" class="img-fluid"></a>
                <div class="p-4 bg-white">
                    <h2 class="h5 text-black mb-3"><strong>Especialización continua</strong></h2>
                    <p class="text-justify">El conocimiento no se detiene, es por eso que estamos en constante actualización para no fallarles a nuestros asociados comerciales.</p>
                </div>
            </div>

            <div class="col-md-12 col-lg-4 mb-5" data-aos="fade-up" data-aos-delay="300">
                <a href="#"><img src="assets/principal/images/topo-geo/carrusel/eje3.jpg" alt="Image" class="img-fluid img-tam"></a>
                <div class="p-4 bg-white">
                    <h2 class="h5 text-black mb-3"><strong href="#">Diversificación de servicios</strong></h2>
                    <p class="text-justify">Ofreces servicios integrales que permitan a nuestros asociados actuales y futuros  contar con la mayor cantidad de información del ámbito de la topografía y de la geodesia para aprovechar los resultados que se les entregue.</p>
                </div>
            </div>
        </div>
    </div>

</div>