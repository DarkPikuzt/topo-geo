
<style>
    .bg-alice{
        background: aliceblue;
    }

</style>

<div class="site-section bg-alice fuente">

	<div class="container">

		<div class="row">

			<div class="col-md-12 ">
				<form id="frmEmail" class="p-5 bg-white border">
					<div class="row form-group" id="valNombreEmail">
						<div class="col-md-12 mb-3 mb-md-0">
							<label class="font-weight-bold" for="fullname">Nombre</label>
							<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre Compreto">
						</div>
					</div>
					<div class="row form-group"  id="valEmail">
						<div class="col-md-12">
							<label class="font-weight-bold" for="email">Email</label>
							<input type="email" id="email" name="email" class="form-control" placeholder="Email ">
						</div>
					</div>
					<div class="row form-group" id="valAsunto">
						<div class="col-md-12">
							<label class="font-weight-bold" for="email">Asunto</label>
							<input type="text" id="asunto" name="asunto" class="form-control" placeholder="Asunto">
						</div>
					</div>
					<div class="row form-group" id="valMensaje">
						<div class="col-md-12">
							<label class="font-weight-bold" for="message">Mensaje</label>
							<textarea  id="mensaje" name="mensaje" cols="30" rows="5" class="form-control" placeholder="Escriba su mensaje"></textarea>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-12">
							<input type="button" value="Enviar" id="btnEmail" class="btn btn-primary  py-2 px-4 rounded-0">
						</div>
					</div>
				
				</form>
			</div>
        </div>
	</div>
</div>