<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/DB.php';

$conexion = new DB();
$conn = $conexion->connection();

$id = $_GET['id'];
$sql = "SELECT * FROM product WHERE id_producto = $id";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);
?>

<style>
    .bg-alice{
        background: aliceblue;
    }

    .btn-posicion{
        left: 12px !important;
        top: -9px !important;
    }

</style>

<div class="site-section site-section-sm bg-alice fuente">
    <div class="container">
        <div class="row bg-white" style="margin-top: 57px">
            <?php foreach ($result  as $row):?>
                <div class="col-lg-4 bg-white">
                    <img src="assets/principal/images/topo-geo/productos/<?php echo $row['img']?>"   alt="Image" class="img-fluid">
                </div>
                <div class="col-lg-8">
                    <div class="bg-white property-body">
                        <div class="row mb-12" >
                            <div class="col-md-6">
                                <h1 class="h4 text-black"><?php echo $row['name_product'] ?></h1>
                            </div>
                        </div>
                        <strong class="text-success h1 mb-3"><?php echo '$'. number_format($row['price'],2)  ?></strong>
                        <br>
                        <h2 class=" d-inline-block text-black mb-0 caption-text">Detalles</h2>
                        <p class="text-justify"> <?php echo $row['description'] ?></p>
                    </div>
                </div>

                <div class="col-md-12 ">
                    <a  href="catalogo.php" class="btn btn-primary btn-posicion">  Regresar</a>
                    <a  href="pdf.php?id=<?php echo $row['id_producto']?>" class="btn btn-primary btn-posicion"> <i class="icon icon-file-pdf-o"></i>
                       Fichero</a>
                </div>

            <?php endforeach; ?>
            <br>



        </div>
    </div>
</div>