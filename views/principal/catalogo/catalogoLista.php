<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();

$sql="

SELECT * FROM product AS P
INNER JOIN category AS C ON 
P.category_id = C.id_category 
INNER JOIN brand AS B ON 
P.brand_id = B.id_brand

";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();
?>
<div class="site-section site-section-sm bg-light">
    <div class="container">
        <?php foreach ($result  as $row):?>
            <div class="row">
                <div class="col-md-12">
                    <div class="property-entry horizontal d-lg-flex">
                        <a href="../../../detalles.php?id=<?php echo $row['id_producto']?>" class="property-thumbnail h-100">
                            <div class="offer-type-wrap">
                                <span class="offer-type bg-info" style="z-index: 1 !important;">Venta</span>
                            </div>
                            <img src="assets/principal/images/topo-geo/productos/<?php echo $row['img']?>" alt="Image" class="img-fluid" width="500 px" height="400px">
                        </a>
                        <div class="p-4 property-body">
                            <a href="../../../detalles.php?id=<?php echo $row['id_producto']?>" class="property-favorite"  ><span class="icon-star_border"></span></a>
                                <h2 class="property-title"><a href="../../../detalles.php?id=<?php echo $row['id_producto']?>"><?php echo strtoupper($row['name_product'])?></a></h2>
                            <span class="property-location d-block mb-3"> <?php echo strtoupper($row['name_brand'])?> </span>
                            <strong class="property-price text-primary mb-3 d-block text-success"><?php echo '$'.number_format($row['price'],2)?></strong>
                            <p><?php echo substr($row['description'],0,150)?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>