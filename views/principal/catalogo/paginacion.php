<div class="col-md-12 bg-alice text-center">
     <nav aria-label="Page navigation example bg-alice">
         <ul class="pagination justify-content-center ">
             <?php if ($total_paginas > 1): ?>
              <?php if ($primera != 1) ?>
                     <li class="page-item <?php echo ($_GET['pagina'] <= 1)? 'disabled' :'' ;?> ">
                 <a class="page-link " href="catalogo.php?pagina=<?php echo $_GET['pagina']-1 ?>" tabindex="-1">Anterior</a>
                 </li>
                 <?php for($i=0; $i < $pagination; $i++): ?>
                     <?php if ($i < 1): ?>
                     <li class="page-item <?php echo ($_GET['pagina']==$i+1)?'active' :'' ;?>"><a class="page-link " href="catalogo.php?pagina=<?php echo $i+1  ?>"><?php echo $i+1?></a></li>
                     <?php else: ?>
                         <li class="page-item <?php echo ($_GET['pagina']==$i+1)?'active' :'' ;?>"><a class="page-link " href="catalogo.php?pagina=<?php echo $i+1  ?>"><?php echo $i+1?></a></li>
                     <?php endif;?>
                 <?php endfor; ?>
             <?php endif;?>
             <li class="page-item <?php echo ($_GET['pagina'] >= $pagination)?'disabled' :'' ;?>">
                 <a class="page-link " href="catalogo.php?pagina=<?php echo $_GET['pagina']+1?>">Siguiente</a>
             </li>
         </ul>
     </nav>
 </div>
