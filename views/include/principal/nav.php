
<style>
    .font{
        font-size: 3vh;
    }

    .fond{
        background: currentColor !important; ;
        top: -24px !important;
    }

    .sticky-top-menu{
        position: sticky;
        top:0;
        z-index: 5;
    }

    .fuente{
    @import url('https://fonts.googleapis.com/css?family=Rubik');
        font-family: 'Rubik', sans-serif;
    }


    @font-face {
        font-family: Harlow Solid Italic ;
        src: url("../../../assets/principal/fonts/HARLOWSI.TTF");
    }
    .fuente-titulo{
        font-family: 'Harlow Solid Italic';
        font-size:  4vh !important;

    }

</style>

<!--menu-->
<div class="site-wrap sticky-top-menu fuente">
	
	<div class="site-mobile-menu">
		<div class="site-mobile-menu-header">
			<div class="site-mobile-menu-close mt-3">
				<span class="icon-close2 js-menu-toggle"></span>
			</div>
		</div>
		<div class="site-mobile-menu-body"></div>
	</div> <!-- .site-mobile-menu -->
    <div class="site-navbar mt-4 fond">
		<div class="container py-1">
			<div class="row align-items-center">
				<div class="col-8 col-md-8 col-lg-3">
					<h1 class="mb-0 fuente-titulo" style="margin-top: -10px"><a href="index.php" class="text-white h2 mb-0 font fuente-titulo"><strong>Topografía y Geodesia Aplicada<span class="text-danger">.</span></strong></a></h1>
				</div>
				<div class="col-4 col-md-4 col-lg-9">
					<nav class="site-navigation text-right text-md-right" role="navigation">
                        <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
                        <ul class="site-menu js-clone-nav d-none d-lg-block">
                            <li><a href="index.php#nosotros">Sobre Nosotros</a></li>
							<li><a href="catalogo.php?pagina=1">Productos/Servicios</a></li>
							<li><a href="index.php#portafolio">Casos de exito</a></li>
							<li><a href="index.php#contacto">Contacto</a></li>
							<li><a href="login.php">Login</a></li>
                        </ul>
					</nav>
				</div>
            </div>
		</div>
	</div>
</div><!--menu-->