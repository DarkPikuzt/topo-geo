<!-- Left side column. contains the logo and sidebar Menu -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="assets/principal/images/topo-geo/logo.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $user?></p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu menu"  data-widget="tree">
                <hr>
                <li class="treeview">
                    <a href="#">
                        <i class="fa  fa-leaf"></i>
                        <span>Productos</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="admin-categoria.php"><i class="fa fa-circle-o"></i>Categoria</a></li>
                        <li><a href="admin-marca.php"><i class="fa fa-circle-o"></i>Marca</a></li>
                        <li><a data-toggle="modal" data-target="#aviso" onclick="dataEditNote('1')"><i class="fa fa-circle-o"></i>Aviso</a></li>
                        <li><a href="admin-productos.php"><i class="fa fa-circle-o"></i>Catalogo de productos</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa  fa-star"></i>
                        <span>Inicio</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="admin-galeria.php"><i class="fa fa-circle-o"></i>Carrusel</a></li>
                        <li><a href="admin-trabajos.php"><i class="fa fa-circle-o"></i>Trabajos</a></li>
                    </ul>
                </li>
                <li class=""><a href="../../../admin-user.php"><i class="fa    fa-users"></i> <span>Usuarios</span></a></li>
                <li class=""><a href="../../../admin-sucursales.php"><i class="fa    fa-users"></i> <span>Sucursales</span></a></li>
            </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/views/admin/productos/avisos.php' ?>


