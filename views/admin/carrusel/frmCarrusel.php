<!-- Modal -->
<div id="editar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Carrusel</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form role="form" id="frmEditCarrusel">
                    <div class="row">
                        <div class="form-group">
                            <label for="descripcion" class="col-sm-2 control-label">Descripción:</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="descripcion" name="descripcion"
                                       placeholder="descripción">
                                <input hidden id="id" name="id">
                            </div>
                        </div>

                        <div class="form-group imgSubida">
                            <label for="imagen" class="col-sm-2 control-label">Imagen:</label>
                            <div class="col-sm-10">
                                <input name="img" type="file" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnEditCarrusel"   data-dismiss="modal" >Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>

    </div>
</div>
