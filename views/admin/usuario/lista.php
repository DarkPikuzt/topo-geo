<?php
require_once$_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();
    $sql="SELECT * FROM user AS U
INNER JOIN admin AS A
ON U.admin_id = A.id_admin";
    $query = $conn->prepare($sql);
    $query->execute();
    $result = $query->fetchAll();
    //var_dump($result);
    ?>


<table id="table_user" class="table table-bordered table-striped text-center">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Usuario</th>
        <th>Editar</th>
        <th>Eliminar</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($result  as $row):?>
        <tr>
            <td><?php echo $row['nombre']," ",$row['apellidoP']," ",$row['apellidoM']?></td>
            <td><?php echo $row['user']?></td>
            <td><button type="button" class="btn btn-info btn-lg btn-sm" data-toggle="modal" data-target="#editar"
                onclick="dataEditUser(
                    '<?php echo $row['nombre']?>',
                        '<?php echo $row['apellidoP']?>',
                        '<?php echo $row['apellidoM']?>',
                        '<?php echo $row['user']?>',
                        '<?php echo $row['id_user']?>',
                        '<?php echo $row['id_admin']?>'
                        )">
                    <i class="fa fa-edit"></i></button></td>
            <td><button class="btn btn-danger btn-lg btn-sm" onclick="eliminar('<?php echo $row['id_admin']?>','1')" ><i class="fa fa-trash-o"></i></button></td>
        </tr>
    <?php endforeach;?>
    </tbody>
    <tfoot>
    <tr>
        <th>Nombre</th>
        <th>Usuario</th>
        <th>Editar</th>
        <th>Eliminar</th>
    </tr>
    </tfoot>
</table>



<script type="application/javascript">
    $(document).ready( function () {
        $('#table_user').DataTable({
            'language': {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    } );
</script>
