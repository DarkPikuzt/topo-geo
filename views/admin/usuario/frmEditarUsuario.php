

<!-- Modal editar Usuarios -->


<!-- Modal -->
<div id="editar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Usuario</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form role="frmEditUser"  id="frmEditUser" >
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group" id="valNom">
                                <label class="control-label" for="titulo">Nombre</label>
                                <input type="text" class="form-control" id="nombreUserU" name="nombreUserU" placeholder="nombre"
                                       required>
                                <input hidden id="idAdmin" name="idAdmin">
                                <input hidden id="idUser" name="idUser">
                                <input hidden id="editar" name="editar" value="1">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="valAP">
                                <label for="titulo">Apellido Paterno</label>
                                <input type="text" class="form-control" id="apellidoPU" name="apellidoPU" placeholder="apellido Paterno">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="valAM">
                                <label for="precio">Apellido Materno</label>
                                <input type="text" class="form-control" id="apellidoMU" name="apellidoMU" placeholder="Apellido Materno">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" id="valUser">
                                <label for="precio">Usuario</label>
                                <input type="text" class="form-control" id="usuarioU" name="usuarioU" placeholder="usuario">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group" >
                                    <label> <strong><input class="spa" type="checkbox" name="frmPass" id="frmPass" checked> </strong>
                                   Cambiar contraseña</label>

                                </div>
                            </div>
                        </div>
                        <div id="inpust">

                            <div class="col-md-12">
                                <div class="form-group"id="valPass">
                                    <label>Nueva contraseña</label>
                                    <div class="input-group" >
                                        <input type="password" class="form-control" id="newPass" name="newPass"placeholder="Contraseña"><span class="input-group-btn">
                                    <span type="button" class="btn btn-info btn-flat" id="show"><i class="fa fa-eye"></i></span>
                                </span>
                                    </div>
                                </div>
                            </div>

                        </div>





                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnEditarUser" name="btnEditarUser" value="Modificar " data-dismiss="modal">Guardar Cambios</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>

    </div>
</div>


