<!-- Modal Usuarios -->
<button type="button" class="btn btn-info btn-lg btn-sm" data-toggle="modal" data-target="#agregar">
    <i class="fa fa-plus-circle"></i> Agregar</button>

<!-- Modal -->
<div id="agregar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Usuario</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form   id="frmAddUser" >
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group" id="valNom">
                                <label class="control-label" for="titulo">Nombre</label>
                                <input type="text" class="form-control" id="nombreUser" name="nombreUser" placeholder="nombre"
                                required>
                                <input hidden id="agregar" name="agregar" value="1">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="valAP">
                                <label for="titulo">Apellido Paterno</label>
                                <input type="text" class="form-control" id="apellidoP" name="apellidoP" placeholder="apellido Paterno">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="valAM">
                                <label for="precio">Apellido Materno</label>
                                <input type="text" class="form-control" id="apellidoM" name="apellidoM" placeholder="Apellido Materno">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="valUser">
                                <label for="precio">Usuario</label>
                                <input type="text" class="form-control" id="usuario" name="usuario" placeholder="usuario">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"id="valPass">
                                <label>Contraseña</label>
                                <div class="input-group" >
                                    <input type="password" class="form-control" id="password" name="password"placeholder="Contraseña"><span class="input-group-btn">
                                    <span type="button" class="btn btn-info btn-flat" id="show"><i class="fa fa-eye"></i></span>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" id="btnAgregar" name="btnAgregar" value="Agregar" data-dismiss="modal"></input>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>

    </div>
</div>