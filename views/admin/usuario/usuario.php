<?php

require_once$_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conn = new DB();
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> Lista de usuarios</h3>
            </div>
<div class="row">
    <div class="col-lg-4 col-md-4 col-md-4 col-md-offset-10">
        <?php require_once 'frmUsuarioA.php'?>
    </div>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <div id="listaUser">
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
    <?php require_once'frmEditarUsuario.php'; ?>
</div>
<!-- /.content-wrapper -->

