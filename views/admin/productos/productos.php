<style>

    .btnModal{
        display: inline-block;
        top: -30px;
        left: 11px;
    }


</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> Lista de Productos</h3>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-md-4 col-md-offset-10   btnModal">
                    <?php require_once 'frmProducto.php'?>
                </div>
                <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/views/admin/productos/avisos.php' ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <div id="listaProductos">
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
    <?php require_once 'frmEditProducto.php';?>

</div>
<!-- /.content-wrapper -->

