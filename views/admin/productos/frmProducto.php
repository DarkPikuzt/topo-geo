<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';
$conexion2 = new DB();
$conn2 =$conexion2->connection();

$sql="SELECT * FROM category ";
$queryC = $conn2->prepare($sql);
$queryC->execute();
$resultC = $queryC->fetchAll();


$sql="SELECT * FROM brand ";
$queryM = $conn2->prepare($sql);
$queryM->execute();
$resultM = $queryM->fetchAll();

?>


<style>

    .btnCategoria{
        position: relative;
        top: 27px;
    }
</style>

<!-- Modal productos -->
<button type="button" class="btn btn-info btn-lg btn-sm btnCategoria" data-toggle="modal" data-target="#agregar">
    <i class="fa fa-plus-circle"></i> Agregar</button>

<!-- Modal -->
<div id="agregar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Producto</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form role="form" id="frmAddProduct">
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group" id="valTitle">
                                <label for="titulo">Titulo</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Titulo">
                                <input hidden id="agregar" name="agregar" value="1">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="valPrice">
                                <label for="precio">Precio</label>
                                <input type="number" class="form-control" id="precio" name="precio" placeholder="precio">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group" id="valCategory">
                                <label>Categoria</label>
                                <select class="form-control" id="categoria" name="categoria">
                                    <option value="" >Seleccione ..</option>
                                    <?php foreach ($resultC  as $rowC):?>
                                        <option value="<?php echo $rowC['id_category']?>" ><?php echo $rowC['name_category']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="valMarca">
                                <label>Marca</label>
                                <select class="form-control" id="marca" name="marca">
                                    <option value="" >Seleccione ..</option>
                                    <?php foreach ($resultM  as $rowM):?>
                                        <option value="<?php echo $rowM['id_brand']?>" ><?php echo $rowM['name_brand']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group" id="valDescription">
                                <label for="precio">descripción</label>
                                <textarea class="form-control rounded" id="descripcion" name="descripcion"  cols="30" rows="10" placeholder="Descripción..."></textarea></div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group" id="valImg">
                                <label for="inputPassword3" class="col-sm-12 control-label">Imagen:</label>
                                <div class="col-sm-10">
                                    <input type="file" id="img" name="img">
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="col-md-12">
                            <div class="form-group" id="valImg">
                                <label for="inputPassword3" class="col-sm-12 control-label">Fichero:</label>
                                <div class="col-sm-10">
                                    <input type="file" id="pdf" name="pdf">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnAddProduct" data-dismiss="modal">Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>