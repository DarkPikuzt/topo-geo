

<!-- Modal -->
<div id="aviso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Aviso</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form role="form" id="frmAddAviso">
                    <div id="dataNote">

                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnAddNote" data-dismiss="modal">Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>
