<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();
$sql="SELECT * FROM product AS P
INNER JOIN category AS C ON 
P.category_id = C.id_category 
INNER JOIN brand AS B ON 
P.brand_id = B.id_brand
 ";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();
//var_dump($result);

$route = $_SERVER['DOCUMENT_ROOT'].'assets/principal/images/topo-geo/carrusel/';

//var_dump($route);
?>

<style>

    .tam-lis{
        max-width: 50px;
        height: 50px;
    }
</style>
<div class="table-responsive">
    <table id="table_Producto" class="table table-bordered table-striped text-center">
        <thead>
        <tr>
            <th>Img</th>
            <th>Nombre Producto</th>
            <th>Precio</th>
            <th>Categoria</th>
            <th>Marca</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result  as $row):?>
            <tr>
                <td><div>
                        <img src="assets/principal/images/topo-geo/productos/<?php echo $row['img']?>" class="img-fluid tam-lis">
                    </div>
                </td>
                <td><?php echo $row['name_product']?></td>
                <td><?php echo $row['price']?></td>
                <td><?php echo $row['name_category']?></td>
                <td><?php echo $row['name_brand']?></td>
                <td><button type="button" class="btn btn-info btn-lg btn-sm" data-toggle="modal" data-target="#editar"
                            onclick="dataEditProduct(
                                    '<?php echo $row['id_producto']?>',
                                    '<?php echo $row['name_product']?>',
                                    '<?php echo $row['price']?>',
                                    '<?php echo $row['category_id']?>',
                                    '<?php echo $row['brand_id']?>'

                                    )">
                        <i class="fa fa-edit"></i></button></td>
                <td><button class="btn btn-danger btn-lg btn-sm" onclick="eliminarProduct('<?php echo $row['id_producto']?>','1')" ><i class="fa fa-trash-o"></i></button></td>

            </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
        <tr>
            <th>Img</th>
            <th>Nombre Producto</th>
            <th>Precio</th>
            <th>Categoria</th>
            <th>Marca</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        </tfoot>
    </table>

</div>


<script type="application/javascript">
    $(document).ready( function () {
        $('#table_Producto').DataTable({
            'language': {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    } );
</script>


