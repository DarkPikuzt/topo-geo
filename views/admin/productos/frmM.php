<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';
$conexion = new DB();
$conn =$conexion->connection();

$id = $_GET['id'];

$sql="SELECT * FROM product WHERE id_producto = $id";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();



$sql="SELECT * FROM category ";
$queryC = $conn->prepare($sql);
$queryC->execute();
$resultC = $queryC->fetchAll();

$sql="SELECT * FROM brand ";
$queryM = $conn->prepare($sql);
$queryM->execute();
$resultM = $queryM->fetchAll();




?>


<div class="box-body">
    <?php foreach ($result  as $row):?>
    <div class="col-md-12">
        <div class="form-group" id="valTitle">
            <label for="titulo">Titulo</label>
            <input type="text" class="form-control" id="tituloU" name="tituloU"
            value="<?php echo $row['name_product']?>">
            <input  id="editar" hidden name="editar" value="1">
            <input  id="id" hidden name="id" value="<?php echo $row['id_producto']?>">

        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group" id="valPrice">
            <label for="precio">Precio</label>
            <input type="number" class="form-control" id="precioU" name="precioU"
                   value="<?php echo $row['price']?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group" id="valCategory">
            <label>Categoria</label>
            <?php $id_categoria = $row['category_id']?>
            <select class="form-control" id="categoriaU" name="categoriaU" value="<?php echo $row['category_id']?>" >

                <?php foreach ($resultC  as $rowC):?>
                <?php if($id_categoria === $rowC['categoria_id']): ?>
                        <option value="<?php echo $rowC['id_category']?>" selected><?php echo $rowC['name_category']?></option>
                    <?php else:; ?>
                        <option value="<?php echo $rowC['id_category']?>" ><?php echo $rowC['name_category']?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group" id="valCategory">
            <label>Marca</label>
            <select class="form-control" id="marcaU" name="marcaU" value="<?php echo $row['brand_id']?>" >
                <?php $id_marca = $row['brand_id']?>
                <?php foreach ($resultM  as $rowM):?>
                <?php if($id_marca === $rowM['brand_id']): ?>
                    <option value="<?php echo $rowM['id_brand']?>" selected><?php echo $rowM['name_brand']?></option>
                <?php else:; ?>
                        <option value="<?php echo $rowM['id_brand']?>"><?php echo $rowM['name_brand']?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group" id="valDescription">
            <label for="precio">descripción</label>
            <textarea class="form-control rounded" id="descripcionU" name="descripcionU"  cols="30" rows="10"
                      ><?php echo $row['description']?></textarea></div>
    </div>
    <div class="col-md-12">
        <div class="form-group" id="valImg">
            <label for="inputPassword3" class="col-sm-12 control-label">Imagen:</label>
            <div class="col-sm-10">
                <input type="file" id="imgU" name="imgU">
            </div>
        </div>
    </div>

        <div class="col-md-12">
            <div class="form-group" id="valImg">
                <label for="inputPassword3" class="col-sm-12 control-label">Fichero:</label>
                <div class="col-sm-10">
                    <input type="file" id="pdfU" name="pdfU">
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>
