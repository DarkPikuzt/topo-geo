<!-- Modal -->
<div id="editar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Marca</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form role="form" id="EditBrand">
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group" id="valMarcaEdit">
                                <input type="text" class="form-control" id="marcaU" name="marcaU" >
                                <input hidden id="id" name="id">
                                <input hidden id="editar" name="editar" value="1">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnEditBrand"  data-dismiss="modal" >Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>

    </div>
</div>
