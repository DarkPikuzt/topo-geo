<style>

    .btnCategoria{
        position: relative;
        top: 27px;
    }
</style>


<!-- Modal productos -->
<button type="button" class="btn btn-info btn-lg btn-sm btnCategoria" tabindex="-1" data-toggle="modal" aria-hidden="true" data-target="#categoria">
    <i class="fa fa-plus-circle"></i> Marca</button>

<!-- Modal -->
<div id="categoria" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Marca</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form role="form" id="frmAddBrand">
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group" id="valMarca">
                                <label for="titulo">Marca</label>
                                <input type="text" class="form-control" id="nombreMarca" name="nombreMarca" placeholder="Marca">
                                <input hidden id="agregar" name="agregar" value="1">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnAddBrand" name="btnAgregar"  data-dismiss="modal" >Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>

    </div>
</div>
