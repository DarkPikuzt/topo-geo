<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();
$sql="SELECT * FROM brand";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();
//var_dump($result);
?>


<table id="table_brand" class="table table-bordered table-striped text-center">
    <thead>
    <tr>
        <th>Marca</th>
        <th>Editar</th>
        <th>Eliminar</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($result  as $row):?>
        <tr>

            <td><?php echo $row['name_brand']?></td>
            <td><button type="button" class="btn btn-info btn-lg btn-sm"
                        data-toggle="modal" data-target="#editar"
                        onclick="dataEditBrand(
                            '<?php echo $row['id_brand']?>',
                            '<?php echo $row['name_brand']?>'
                            )">
                    <i class="fa fa-edit"></i></button>
            </td>
            <td><button class="btn btn-danger btn-lg btn-sm"
                        onclick="eliminarBrand('<?php echo $row['id_brand'] ?>','1')">
                    <i class="fa fa-edit"></i></button></td>

        </tr>
    <?php endforeach;?>
    </tbody>
    <tfoot>
    <tr>
        <th>Marca</th>
        <th>Editar</th>
        <th>Eliminar</th>
    </tr>
    </tfoot>
</table>



<script type="application/javascript">
    $(document).ready( function () {
        $('#table_brand').DataTable({
            'language': {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    } );
</script>
