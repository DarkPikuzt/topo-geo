<?php
require_once '../../../config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();
$sql="SELECT * FROM category";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();
//var_dump($result);
?>


<table id="table_category" class="table table-bordered table-striped text-center">
    <thead>
    <tr>
        <th>Categoria</th>
        <th>Editar</th>
        <th>Eliminar</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($result  as $row):?>
        <tr>

            <td><?php echo $row['name_category']?></td>
            <td><button type="button" class="btn btn-info btn-lg btn-sm"
                        data-toggle="modal" data-target="#editar"
                onclick="dataEditCategory(
                    '<?php echo $row['id_category']?>',
                    '<?php echo $row['name_category']?>'
                )">
                    <i class="fa fa-edit"></i></button>
            </td>
            <td><button class="btn btn-danger btn-lg btn-sm"
                onclick="eliminarCategory('<?php echo $row['id_category'] ?>','1')">
                    <i class="fa fa-edit"></i></button></td>

        </tr>
    <?php endforeach;?>
    </tbody>
    <tfoot>
    <tr>
        <th>Categoria</th>
        <th>Editar</th>
        <th>Eliminar</th>
    </tr>
    </tfoot>
</table>



<script type="application/javascript">
    $(document).ready( function () {
        $('#table_category').DataTable({
            'language': {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    } );
</script>
