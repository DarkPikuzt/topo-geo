<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();
$sql="select * from branch_office ";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();
//var_dump($result);

$route = $_SERVER['DOCUMENT_ROOT'].'assets/principal/images/topo-geo/carrusel/';

//var_dump($route);
?>

<style>
    .tam-lis{
        max-width: 50px;
        height: 50px;
    }
</style>
<div class="table-responsive">
    <table id="table_suc" class="table table-bordered table-striped text-center">
        <thead>
        <tr>
            <th>Nombre Sucursal</th>
            <th>Calle</th>
            <th>Colonia</th>
            <th>Codigo Postal</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result  as $row):?>
            <tr>

                <td><?php echo $row['name_office']?></td>
                <td><?php echo $row['street_office']?></td>
                <td><?php echo $row['colony_office']?></td>
                <td><?php echo $row['cp_office']?></td>


                <td><button type="button" class="btn btn-info btn-lg btn-sm" data-toggle="modal" data-target="#editar"
                            onclick="dataEditSucursal(
                                '<?php echo $row['id_office']?>',
                                '<?php echo $row['name_office']?>',
                                '<?php echo $row['street_office']?>',
                                '<?php echo $row['colony_office']?>',
                                '<?php echo $row['cp_office']?>'
                                )">
                        <i class="fa fa-edit"></i></button></td>
                <td><button class="btn btn-danger btn-lg btn-sm" onclick="eliminarSucursal('<?php echo $row['id_office']?>','1')" ><i class="fa fa-trash-o"></i></button></td>
            </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
        <tr>
            <th>Nombre Sucursal</th>
            <th>Calle</th>
            <th>Colonia</th>
            <th>Codigo Postal</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        </tfoot>
    </table>

</div>


<script type="application/javascript">


    $(document).ready( function () {
        $('#table_suc').DataTable({
            'language': {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    } );


</script>
