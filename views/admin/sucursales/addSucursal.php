<!-- Modal Usuarios -->
<button type="button" class="btn btn-info btn-lg btn-sm" data-toggle="modal" data-target="#agregar">
    <i class="fa fa-plus-circle"></i> Agregar</button>

<!-- Modal -->
<div id="agregar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Sucursales</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <form role="form" id="frmAddSucursal">
                        <div class="box-body">

                            <div class="col-md-6">
                                <div class="form-group" id="valnombre">
                                    <label for="titulo">Nombre Sucursal:</label>
                                    <input type="text" class="form-control" id="nombreS" name="nombreS" placeholder="nombre sucursal"|>
                                </div>
                            </div>

                        <div class="col-md-6">
                            <div class="form-group" id="valCal">
                                <label class="control-label" for="titulo">Calle:</label>
                                <input type="text" class="form-control" id="calleSu" name="calleSu" placeholder="Calle sucursal"
                                required>
                                <input hidden id="agregar" name="agregar" value="1">
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group" id="valCol">
                                <label for="titulo">Colonia:</label>
                                <input type="text" class="form-control" id="coloniaSu" name="coloniaSu" placeholder="colonia Sucursal"|>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="valCP">
                                <label for="precio">Codigo Postal:</label>
                                <input type="number" class="form-control" id="codigoSu" name="codigoSu" placeholder="CP">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group" id="valEnl">
                                <label for="precio">Enlace mapa</label>
                                <input type="text" class="form-control" id="enlaceM" name="enlaceM" placeholder="Enlace mapa">
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" id="btnAgregarSucursal" name="btnAgregar" value="Agregar" data-dismiss="modal"></input>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>

    </div>
</div>