<?php
require_once$_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

$conexion = new DB();
$conn =$conexion->connection();
$sql="SELECT * FROM portfolio ";
$query = $conn->prepare($sql);
$query->execute();
$result = $query->fetchAll();
//var_dump($result);
$route = $_SERVER['DOCUMENT_ROOT'].'/assets/principal/images/topo-geo/carrusel/';


?>

<style>
    .tam-lis{
        max-width: 50px;
        height: 50px;
    }
</style>


<table id="table_trabajos" class="table table-bordered table-striped text-center">
    <thead>
    <tr>
        <th>Img</th>
        <th>Titulo</th>
        <th>localización</th>
        <th>Editar</th>

    </tr>
    </thead>
    <tbody>
    <?php foreach ($result  as $row):?>
        <tr>
            <td><div>
                    <img src=" assets/principal/images/topo-geo/carrusel/<?php echo $row['img']?>" class="img-fluid tam-lis">
                </div>
            </td>
            <td><?php echo $row['title']?></td>
            <td><?php echo $row['location']?></td>
            <td><button type="button" class="btn btn-info btn-lg btn-sm" data-toggle="modal" data-target="#editar"
                onclick="dataEditWord(
                    '<?php echo $row['id_porfolio']?>',
                    '<?php echo $row['title']?>',
                    '<?php echo $row['location']?>',
                    '<?php echo $row['description']?>'
                        )">
                <i class="fa fa-edit"></i></button></td>
         </tr>
    <?php endforeach;?>
    </tbody>
    <tfoot>
    <tr>
        <th>Img</th>
        <th>Titulo</th>
        <th>localización</th>
        <th>Editar</th>

    </tr>
    </tfoot>
</table>



<script type="application/javascript">
    $(document).ready( function () {
        $('#table_trabajos').DataTable({
            'language': {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
    } );
</script>
