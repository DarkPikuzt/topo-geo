<!-- Modal -->
<div id="editar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Trabajos</h4>
            </div>
            <div class="modal-body">
                <!--formulario-->
                <form role="form" id="frmEditWord">
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group" id="valTitle">
                                <label for="titulo">Titulo</label>
                                <input type="text" class="form-control" id="tituloUW" name="tituloUW" placeholder="Titulo">
                                <input hidden id="id"  name="id">

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="valLugar">
                                <label for="precio">Localización</label>
                                <input type="text" class="form-control" id="lugarUW" name="lugarUW" placeholder="precio">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" id="valDescription">
                                <label for="precio">Descripción</label>
                                <textarea class="form-control rounded" id="descripcionUW" name="descripcionUW"  cols="30" rows="10" placeholder="Descripción..."></textarea></div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" id="valImg">
                                <label for="inputPassword3" class="col-sm-12 control-label">Imagen:</label>
                                <div class="col-sm-10">
                                    <input type="file" id="img" name="img">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnEditWord" data-dismiss="modal">Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
            </div>
        </div>

    </div>
</div>
