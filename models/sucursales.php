<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';


class sucursales
{
    public $calle;
    public $colonia;
    public $CP;
    public $enlace;
    public $nombre;
    public $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * @param mixed $calle
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;
    }

    /**
     * @return mixed
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * @param mixed $colonia
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;
    }

    /**
     * @return mixed
     */
    public function getCP()
    {
        return $this->CP;
    }

    /**
     * @param mixed $CP
     */
    public function setCP($CP)
    {
        $this->CP = $CP;
    }

    /**
     * @return mixed
     */
    public function getEnlace()
    {
        return $this->enlace;
    }

    /**
     * @param mixed $enlace
     */
    public function setEnlace($enlace)
    {
        $this->enlace = $enlace;
    }

    public function SucursalRepetida(){
        $conexion = new DB();
        $conn = $conexion->connection();
        try{
            $namecategory = $this->getNombre();
            //$pass = $this->getPassword();

            $sql = "SELECT name_office FROM branch_office WHERE name_office = '$namecategory'";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            //var_dump($result);
            return $result;

        }catch (PDOException $e){
            echo  'ERROR:'.$e->getMessage();
            die();
        }
    }

    public function addSucursal(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $nombre = $this->getNombre();
        $calle = $this->getCalle();
        $colonia = $this->getColonia();
        $cp = $this->getCP();
        $enlace = $this->getEnlace();

        try{
            $sql = "INSERT INTO branch_office 
                    (name_office,
                    street_office,
                    colony_office,
                    cp_office,
                    frame_office
                    ) 
                    VALUES 
                    (?,?,?,?,?)";

            $query = $conn->prepare($sql);
            $query->bindParam(1,$nombre);
            $query->bindParam(2,$calle);
            $query->bindParam(3,$colonia);
            $query->bindParam(4,$cp);
            $query->bindParam(5,$enlace);
            $result = $query->execute();
            return $result;
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }

    public function editSuc(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nombre = $this->getNombre();
        $calle = $this->getCalle();
        $colonia = $this->getColonia();
        $cp = $this->getCP();
        $enlace = $this->getEnlace();
        $id = $this->getId();

        //var_dump($id);
        try{
            $sql ="
            UPDATE branch_office SET
            name_office = ?,       
            street_office = ?,       
            colony_office = ?,       
            cp_office = ?,       
            frame_office = ?      
            WHERE id_office = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$nombre);
            $query->bindValue(2,$calle);
            $query->bindValue(3,$colonia);
            $query->bindValue(4,$cp);
            $query->bindValue(5,$enlace);
            $query->bindValue(6,$id);

            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }


    public function deleteSucursal(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getId();
        //var_dump($idAdmin);

        try{
            $sql = "DELETE FROM branch_office WHERE id_office = ?";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$id);
            $result = $query->execute();

            echo $result;

        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }




}