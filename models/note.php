<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

class note
{
    public $note;
    public $id;

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    public function addNote(){

    }

    public function updateNote(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $note = $this->getNote();
        $id = $this->getId();
        //var_dump($nameImg);

        try{
            $sql ="
            UPDATE note SET
            note = ?
            WHERE id_note = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$note);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }


}