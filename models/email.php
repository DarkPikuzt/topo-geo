<?php


class email
{
    public $email;
    public $affair;
    public $messege;
    public $name;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAffair()
    {
        return $this->affair;
    }

    /**
     * @param mixed $affair
     */
    public function setAffair($affair)
    {
        $this->affair = $affair;
    }

    /**
     * @return mixed
     */
    public function getMessege()
    {
        return $this->messege;
    }

    /**
     * @param mixed $messege
     */
    public function setMessege($messege)
    {
        $this->messege = $messege;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function submitEmail(){
        $destino = 'tgapventasxal@telmexmail.com';
        $asunto = $this->getAffair();
        $email = $this->getEmail();
        $messenge = $this->getMessege();
        $name = $this->getName();


        $carta = "De: $name \n";
        $carta .="Correo: $email \n";
        $carta .="Mensaje: $messenge";
        $verify = mail($destino,$asunto,$carta);

        if($verify){
            return 1;

        }else{
            return 0;
        }
    }


}