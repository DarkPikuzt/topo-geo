<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';

class porfolio
{

    public $title;
    public $description;
    public $typeImg;
    public $sizeImg;
    public $tempdestino;
    public $destination;
    public $newNameImg;
    public $idPorfolio;
    public $place;
    public $nameImg;

    /**
     * @return mixed
     */
    public function getNameImg()
    {
        return $this->nameImg;
    }

    /**
     * @param mixed $nameImg
     */
    public function setNameImg($nameImg)
    {
        $this->nameImg = $nameImg;
    }



    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }


    /**
     * @return mixed
     */
    public function getIdPorfolio()
    {
        return $this->idPorfolio;
    }

    /**
     * @param mixed $idPorfolio
     */
    public function setIdPorfolio($idPorfolio)
    {
        $this->idPorfolio = $idPorfolio;
    }



    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTypeImg()
    {
        return $this->typeImg;
    }

    /**
     * @param mixed $typeImg
     */
    public function setTypeImg($typeImg)
    {
        $this->typeImg = $typeImg;
    }

    /**
     * @return mixed
     */
    public function getSizeImg()
    {
        return $this->sizeImg;
    }

    /**
     * @param mixed $sizeImg
     */
    public function setSizeImg($sizeImg)
    {
        $this->sizeImg = $sizeImg;
    }

    /**
     * @return mixed
     */
    public function getTempdestino()
    {
        return $this->tempdestino;
    }

    /**
     * @param mixed $tempdestino
     */
    public function setTempdestino($tempdestino)
    {
        $this->tempdestino = $tempdestino;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return mixed
     */
    public function getNewNameImg()
    {
        return $this->newNameImg;
    }

    /**
     * @param mixed $newNameImg
     */
    public function setNewNameImg($newNameImg)
    {
        $this->newNameImg = $newNameImg;
    }


    public function updatePorfolio(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $descripcion = $this->getDescription();
        $title = $this->getTitle();
        $location = $this->getPlace();
        //var_dump($descripcion);
        $id = $this->getIdPorfolio();

        //var_dump($id);

        try{
            $sql ="
            UPDATE portfolio SET
            title = ?,
            location = ?,
            description = ?            
            WHERE id_porfolio = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$title);
            $query->bindValue(2,$location);
            $query->bindValue(3,$descripcion);
            $query->bindValue(4,$id);
            $result = $query->execute();

            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }

    }
    public function updateImage(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $nameImg = $this->getNewNameImg();
        $id = $this->getIdPorfolio();

        //var_dump($nameImg);

        try{
            $sql ="
            UPDATE portfolio SET
            img = ?
            WHERE id_porfolio = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$nameImg);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }
    public function deleteFileImg(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getIdPorfolio();
        $destination= $this->getDestination();
        //var_dump($destination);
        try{
            $sql ="SELECT img FROM portfolio WHERE id_porfolio = $id ";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            $route = $destination.$result;


            //var_dump($route);
            //unlink($route);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }
}