<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';


class product
{

    public $title;
    public $price;
    public $description;
    public $category;
    public $img;
    public $nameImg;
    public $idImg;
    public $typeImg;
    public $sizeImg;
    public $tempdestino;
    public $destination;
    public $newNameImg;
    public $idCategory;
    public $id;
    public $idbrand;
    public $pdf;
    public $destinationPdf;
    public $tempPdf;
    public $typePdf;

    /**
     * @return mixed
     */
    public function getTypePdf()
    {
        return $this->typePdf;
    }

    /**
     * @param mixed $typePdf
     */
    public function setTypePdf($typePdf)
    {
        $this->typePdf = $typePdf;
    }



    /**
     * @return mixed
     */
    public function getDestinationPdf()
    {
        return $this->destinationPdf;
    }

    /**
     * @param mixed $destinationPdf
     */
    public function setDestinationPdf($destinationPdf)
    {
        $this->destinationPdf = $destinationPdf;
    }

    /**
     * @return mixed
     */
    public function getTempPdf()
    {
        return $this->tempPdf;
    }

    /**
     * @param mixed $tempPdf
     */
    public function setTempPdf($tempPdf)
    {
        $this->tempPdf = $tempPdf;
    }



    /**
     * @return mixed
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param mixed $pdf
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }





    /**
     * @return mixed
     */
    public function getIdbrand()
    {
        return $this->idbrand;
    }

    /**
     * @param mixed $idbrand
     */
    public function setIdbrand($idbrand)
    {
        $this->idbrand = $idbrand;
    }






    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    /**
     * @return mixed
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * @param mixed $idCategory
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    }



    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


    /**
     * @return mixed
     */
    public function getNameImg()
    {
        return $this->nameImg;
    }

    /**
     * @param mixed $nameImg
     */
    public function setNameImg($nameImg)
    {
        $this->nameImg = $nameImg;
    }

    /**
     * @return mixed
     */
    public function getIdImg()
    {
        return $this->idImg;
    }

    /**
     * @param mixed $idImg
     */
    public function setIdImg($idImg)
    {
        $this->idImg = $idImg;
    }

    /**
     * @return mixed
     */
    public function getTypeImg()
    {
        return $this->typeImg;
    }

    /**
     * @param mixed $typeImg
     */
    public function setTypeImg($typeImg)
    {
        $this->typeImg = $typeImg;
    }

    /**
     * @return mixed
     */
    public function getSizeImg()
    {
        return $this->sizeImg;
    }

    /**
     * @param mixed $sizeImg
     */
    public function setSizeImg($sizeImg)
    {
        $this->sizeImg = $sizeImg;
    }

    /**
     * @return mixed
     */
    public function getTempdestino()
    {
        return $this->tempdestino;
    }

    /**
     * @param mixed $tempdestino
     */
    public function setTempdestino($tempdestino)
    {
        $this->tempdestino = $tempdestino;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return mixed
     */
    public function getNewNameImg()
    {
        return $this->newNameImg;
    }

    /**
     * @param mixed $newNameImg
     */
    public function setNewNameImg($newNameImg)
    {
        $this->newNameImg = $newNameImg;
    }



    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    public function addProducto(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameProduct = $this->getTitle();
        $priceProduct = $this->getPrice();
        $descriptionProducto = $this->getDescription();
        $nameImg = $this->getNewNameImg();
        $idC = $this->getIdCategory();
        $brand =$this->getIdbrand();
        $namePdf = $this->getPdf();
        try{
            $sql = "INSERT INTO product 
                    (name_product,
                    description,
                    price,
                    img,
                    category_id,
                    brand_id,
                    pdf
                    ) 
                    VALUES 
                    (?,?,?,?,?,?,?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$nameProduct);
            $query->bindParam(2,$descriptionProducto);
            $query->bindParam(3,$priceProduct);
            $query->bindParam(4,$nameImg);
            $query->bindParam(5,$idC);
            $query->bindParam(6,$brand);
            $query->bindParam(7,$namePdf);
            $result = $query->execute();
            return $result;
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }

    public function updateProducto(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $nameProduct = $this->getTitle();
        $priceProduct = $this->getPrice();
        $descriptionProducto = $this->getDescription();
        $idBrand = $this->getIdbrand();
        $idC = $this->getIdCategory();
        $id =$this->getId();



        try{
            $sql ="
            UPDATE product SET
            name_product = ?,
            description = ?,
            price = ?,
            category_id = ?,
            brand_id = ?
            WHERE id_producto = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$nameProduct);
            $query->bindValue(2,$descriptionProducto);
            $query->bindValue(3,$priceProduct);
            $query->bindValue(4,$idC);
            $query->bindValue(5,$idBrand);
            $query->bindValue(6,$id);
            $result = $query->execute();

            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }

    }

    public function updateImg(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameImg = $this->getNewNameImg();
        $id =$this->getId();

        try{
            $sql ="
            UPDATE product SET
            img = ?            
            WHERE id_producto = ?
            ";

            $query = $conn->prepare($sql);
            $query->bindValue(1,$nameImg);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }


    }

    public function deleteFileImg(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getId();
        $destination= $this->getDestination();
        try{
            $sql ="SELECT img FROM product WHERE id_producto = $id ";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            $route = $destination.$result;
            //var_dump($route);
            unlink($route);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }

    public function deleteProduct(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getId();
        //var_dump($idAdmin);
        try{
            $sql = "DELETE FROM product WHERE id_producto = ?";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$id);
            $result = $query->execute();

            return $result;

        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }

    public function updatePdf (){
        $conexion = new DB();
        $conn = $conexion->connection();
        $namePdf = $this->getPdf();
        $id =$this->getId();
        try{
            $sql ="
            UPDATE product SET
            pdf = ?            
            WHERE id_producto = ?
            ";

            $query = $conn->prepare($sql);
            $query->bindValue(1,$namePdf);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }

    public function deleteFilePdf(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getId();
        $destination= $this->getDestinationPdf();
        try{
            $sql ="SELECT pdf FROM product WHERE id_producto = $id ";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            $route = $destination.$result;
            //var_dump($route);
            unlink($route);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }



}