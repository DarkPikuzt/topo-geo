<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';


class brand
{

    public $nameBrand;
    public $idBrand;

    /**
     * @return mixed
     */
    public function getNameBrand()
    {
        return $this->nameBrand;
    }

    /**
     * @param mixed $nameBrand
     */
    public function setNameBrand($nameBrand)
    {
        $this->nameBrand = $nameBrand;
    }

    /**
     * @return mixed
     */
    public function getIdBrand()
    {
        return $this->idBrand;
    }

    /**
     * @param mixed $idBrand
     */
    public function setIdBrand($idBrand)
    {
        $this->idBrand = $idBrand;
    }

    public function addBrand(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameBrand = $this->getNameBrand();

        try{
            $sql = "INSERT INTO brand (name_brand) VALUES (?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$nameBrand);
            $result = $query->execute();
            return $result;

        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }
    public function checkBrand(){
        $conexion = new DB();
        $conn = $conexion->connection();
        try{
            $nameBrand = $this->getNameBrand();
            $sql = "SELECT name_brand FROM brand WHERE name_brand = '$nameBrand'";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();

            return $result;

        }catch (PDOException $e){
            echo  'ERROR:'.$e->getMessage();
            die();
        }
    }
    public function updateBrand(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameBrand= $this->getNameBrand();
        $id = $this->getIdBrand();
        try{
            $sql ="
            UPDATE brand SET
            name_brand = ?            
            WHERE id_brand = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$nameBrand);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($result);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }
    public function deleteBrand(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getIdBrand();
        //var_dump($idAdmin);

        try{
            $sql = "DELETE FROM brand WHERE id_brand = ?";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$id);
            $result = $query->execute();
            return $result;

            //var_dump($result);
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }



}