<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/DB.php';
/**
 * Created by PhpStorm.
 * User: Pikuzt
 * Date: 22/3/2019
 * Time: 13:04
 */

class usuarios extends DB
{

    public $name;
    public $lastName;
    public $SecundLastName;
    public $user;
    public $password;
    public $status;
    public $newPassword;
    public $idUser;
    public $idAdmin;

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }
    /**
     * @return mixed
     */
    public function getIdAdmin()
    {
        return $this->idAdmin;
    }
    /**
     * @param mixed $idAdmin
     */
    public function setIdAdmin($idAdmin)
    {
        $this->idAdmin = $idAdmin;
    }
    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }
    /**
     * @param mixed $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    /**
     * @return mixed
     */
    public function getSecundLastName()
    {
        return $this->SecundLastName;
    }
    /**
     * @param mixed $SecundLastName
     */
    public function setSecundLastName($SecundLastName)
    {
        $this->SecundLastName = $SecundLastName;
    }
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
    /*Insertamos en la base datos*/
    public function insertUser (){
        $conexion = new DB();
        $conn = $conexion->connection();

        $name = $this->getName();
        $lastName = $this->getLastName();
        $secundLastName = $this->getSecundLastName();
        $user = $this->getUser();
        $hash = $this->getPassword();
        $pass = password_hash($hash,PASSWORD_DEFAULT);
        try{
            $sql = "INSERT INTO admin (nombre, apellidoP, apellidoM) VALUES (?, ?, ?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$name);
            $query->bindParam(2,$lastName);
            $query->bindParam(3,$secundLastName);
            $result = $query->execute();
            $id = $conn->lastInsertId();
            $dataSession = array(
                'user'=> $user,
                'pass'=> $pass,
                'id' => $id
            );
            $session = self::userSession($dataSession);
                if(($result == true)&&($session == true)){
                    return 1;
                }else{
                    return 0;
                }

        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }
    public function userSession($data){
        $conexion = new DB();
        $conn = $conexion->connection();
        try{
            $sql ='INSERT INTO user (admin_id,user, password) VALUES (?, ?, ?)';
            $query = $conn->prepare($sql);
            $query -> bindParam(1,$data['id']);
            $query ->bindParam(2,$data['user']);
            $query ->bindParam(3,$data['pass']);
            $result =$query->execute();
            return $result;
        }catch (PDOException $e){
            echo 'ERROR:'. $e->getMessage();
            die();
        }
    }
    public function updateAdmin(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $name = $this->getName();
        $lastName = $this->getLastName();
        $secundLastName = $this->getSecundLastName();
        $idAdmin = $this->getIdAdmin();


        try{
            $sql ="
            UPDATE admin SET
            nombre =?,
            apellidoP = ?,
            apellidoM = ?
            WHERE id_admin = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$name);
            $query->bindValue(2,$lastName);
            $query->bindValue(3,$secundLastName);
            $query->bindValue(4,$idAdmin);
            $result = $query->execute();
            //var_dump($result);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }
    public function updateNameUserAndAdmin(){
        $updateAdmin = self::updateAdmin();
        $updateNameUser = self::updateNameUser();
        if ( ($updateNameUser >0)&&($updateAdmin >0)){
            return 1;

        }else{
            return 0;
        }

    }
    public function updateNameUser(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $idUser = $this->getIdUser();
        $nameUser = $this->getUser();

        try{
            $sql = "
             UPDATE user SET
            user = ?           
            WHERE id_user = ?
            ";
            $query = $conn->prepare($sql);
            $query ->bindValue(1,$nameUser);
            $query ->bindValue(2,$idUser);
            $result = $query->execute();
            return $result;
        }catch (PDOException $e){
            echo 'ERRO:'.$e;
        }
    }
    public function editUserPass(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $newPass = password_hash($this->getNewPassword(), PASSWORD_DEFAULT);
        $idUser = $this->getIdUser();
        try{
            $sql ="
            UPDATE user SET            
            password = ?
            WHERE id_user = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$newPass);
            $query->bindValue(2,$idUser);
            $result = $query->execute();

           // var_dump($result);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
        }
    }
    public function deleteUser(){

    }
    /*mostramos la base de datos*/
    public function showUser(){
        $conexion = new DB();
        $conn = $conexion->connection();
        try{

            $sql="SELECT * FROM admin";
            $query = $conn->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            return $result;


        }catch (PDOException $e){
            echo "Error:". $e->getMessage();
        }



    }
    public function userRepet ($user){
        $conexion = new DB();
        $conn = $conexion->connection();
        try{
            $sql = "SELECT user FROM user WHERE user = '$user'";
            $query = $conn->query($sql);
            $count = $query->fetchColumn();
            //var_dump($count);
            if( $count === $user ){
                return 1;
            }else{
                return 0;
            }
        }catch (PDOException $e){
            echo "ERROR:".$e->getMessage();
        }


    }
    public  function checkPass(){
        $conexion = new DB();
        $conn = $conexion->connection();

        try{
            $user = $this->getUser();
            $pass = $this->getPassword();

            $sql = "SELECT password FROM user WHERE user = '$user'";
            $query = $conn->query($sql);
            $verify = $query->fetchColumn();

            if(password_verify($pass,$verify)){
                return 1;
            }else{
                return 0;
            }
        }catch (PDOException $e){
            echo  'ERROR:'.$e->getMessage();
        }
    }
    public function deleteAdminAndUser(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $idAdmin = $this->getIdAdmin();
        //var_dump($idAdmin);
        try{
            $sql = "DELETE FROM admin WHERE id_admin = ?";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$idAdmin);
            $result = $query->execute();

            return $result;

        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }
    public function loginUser(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $user = $this->getIdUser();
        $pass =$this->getPassword();

        try{
            $sql= "
            SELECT user,password FROM user AS U
                INNER JOIN admin AS A
                ON U.admin_id = A.id_admin
                WHERE USER = '$user'
            ";

        }catch (PDOException $e){
            echo 'ERROR'.$e;
        }
    }
    public function sessionUser(){

        //session_start();
        $conexion = new DB();
        $conn = $conexion->connection();
        $user = $this->getUser();
        $_SESSION['session'] = $user;

        try{
            $user = $this->getUser();
            $pass = $this->getPassword();

            $sql = "SELECT password FROM user WHERE user = '$user'";
            $query = $conn->query($sql);
            $verify = $query->fetchColumn();

            if(password_verify($pass,$verify)){
                return 1;
            }else{
                return 0;
            }
        }catch (PDOException $e){
            echo  'ERROR:'.$e->getMessage();
        }
        //var_dump($_SESSION['User']);
    }

}