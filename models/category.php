<?php
require_once '../config/DB.php';

class category
{

    public $nameCategory;
    public $idCategory;

    /**
     * @return mixed
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * @param mixed $idCategory
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    }



    /**
     * @return mixed
     */
    public function getNameCategory()
    {
        return $this->nameCategory;
    }

    /**
     * @param mixed $nameCategory
     */
    public function setNameCategory($nameCategory)
    {
        $this->nameCategory = $nameCategory;
    }


    public function addCategory(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameCategory = $this->getNameCategory();
        //var_dump($nameCategory);
        try{
            $sql = "INSERT INTO category (name_category) VALUES (?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$nameCategory);
            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }

    }
    public function checkCategory(){
        $conexion = new DB();
        $conn = $conexion->connection();
        try{
            $namecategory = $this->getNameCategory();
            //$pass = $this->getPassword();

            $sql = "SELECT name_category FROM category WHERE name_category = '$namecategory'";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            //var_dump($result);
            return $result;

        }catch (PDOException $e){
            echo  'ERROR:'.$e->getMessage();
            die();
        }
    }

    public function editCategory(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $category= $this->getNameCategory();
        $id = $this->getIdCategory();
        try{
            $sql ="
            UPDATE category SET
            name_category = ?            
            WHERE id_category = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$category);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($result);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }
    public function deleteCategory(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getIdCategory();
        //var_dump($idAdmin);

        try{
            $sql = "DELETE FROM category WHERE id_category = ?";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$id);
            $result = $query->execute();
            return $result;

            //var_dump($result);
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }



}