<?php
require_once '../config/DB.php';


class carrusel
{
    public $descrition;
    public $nameImg;
    public $idImg;
    public $typeImg;
    public $sizeImg;
    public $tempdestino;
    public $destination;
    public $newNameImg;

    /**
     * @return mixed
     */
    public function getNewNameImg()
    {
        return $this->newNameImg;
    }

    /**
     * @param mixed $newNameImg
     */
    public function setNewNameImg($newNameImg)
    {
        $this->newNameImg = $newNameImg;
    }



    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }



    /**
     * @return mixed
     */
    public function getTempdestino()
    {
        return $this->tempdestino;
    }

    /**
     * @param mixed $tempdestino
     */
    public function setTempdestino($tempdestino)
    {
        $this->tempdestino = $tempdestino;
    }



    /**
     * @return mixed
     */
    public function getTypeImg()
    {
        return $this->typeImg;
    }

    /**
     * @param mixed $typeImg
     */
    public function setTypeImg($typeImg)
    {
        $this->typeImg = $typeImg;
    }

    /**
     * @return mixed
     */
    public function getSizeImg()
    {
        return $this->sizeImg;
    }

    /**
     * @param mixed $sizeImg
     */
    public function setSizeImg($sizeImg)
    {
        $this->sizeImg = $sizeImg;
    }



    /**
     * @return mixed
     */
    public function getIdImg()
    {
        return $this->idImg;
    }

    /**
     * @param mixed $idImg
     */
    public function setIdImg($idImg)
    {
        $this->idImg = $idImg;
    }





    /**
     * @return mixed
     */
    public function getDescrition()
    {
        return $this->descrition;
    }

    /**
     * @param mixed $descrition
     */
    public function setDescrition($descrition)
    {
        $this->descrition = $descrition;
    }

    /**
     * @return mixed
     */
    public function getNameImg()
    {
        return $this->nameImg;
    }

    /**
     * @param mixed $nameImg
     */
    public function setNameImg($nameImg)
    {
        $this->nameImg = $nameImg;
    }


    public function UpdateCarrusel(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $descripcion = $this->getDescrition();
        //var_dump($descripcion);
        $id = $this->getIdImg();

        //var_dump($id);

        try{
            $sql ="
            UPDATE gallery SET
            description = ?
            WHERE id_gallery = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$descripcion);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }

    }
    public function updateImage(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameImg = $this->getNewNameImg();
        $id = $this->getIdImg();

        //var_dump($id);

        try{
            $sql ="
            UPDATE gallery SET
            nameImg = ?
            WHERE id_gallery = ?
            ";
            $query = $conn->prepare($sql);
            $query->bindValue(1,$nameImg);
            $query->bindValue(2,$id);
            $result = $query->execute();
            //var_dump($sql);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }
    public function deleteFileImg(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getIdImg();
        $destination= $this->getDestination();
        try{
            $sql ="SELECT nameImg FROM gallery WHERE id_gallery = $id ";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            $route = $destination.$result;
            //var_dump($route);


            unlink($route);
            return $result;
        }catch (PDOException $e){
            echo 'ERROR'.$e;
            die();
        }
    }









}