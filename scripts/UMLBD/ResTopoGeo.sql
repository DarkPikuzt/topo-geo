/*
SQLyog Community v13.1.2 (64 bit)
MySQL - 5.7.24 : Database - dbtopogeo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  `apellidoP` varchar(15) DEFAULT NULL,
  `apellidoM` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id_admin`,`nombre`,`apellidoP`,`apellidoM`) values 
(4,'NA','NA','Na');

/*Table structure for table `brand` */

DROP TABLE IF EXISTS `brand`;

CREATE TABLE `brand` (
  `id_brand` int(11) NOT NULL AUTO_INCREMENT,
  `name_brand` char(150) DEFAULT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `brand` */

insert  into `brand`(`id_brand`,`name_brand`) values 
(2,'TOPCON'),
(3,'SOKKIA'),
(4,'SPECTRA PRECISION'),
(11,'modificadocds'),
(17,'muestras');

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name_category` char(100) DEFAULT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert  into `category`(`id_category`,`name_category`) values 
(8,'Madera'),
(9,'Fierro'),
(10,'Plastico');

/*Table structure for table `gallery` */

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `description` char(150) DEFAULT NULL,
  `nameImg` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `gallery` */

insert  into `gallery`(`id_gallery`,`description`,`nameImg`) values 
(1,'Fomentamos el trabajo en equipo.','imagen_1622483318.jpg'),
(2,'Creando sinergia con nuestros clientes.','imagen_1015520139.jpg'),
(3,'Caminando hacia el futuro.','imagen_1015535780.jpg');

/*Table structure for table `image` */

DROP TABLE IF EXISTS `image`;

CREATE TABLE `image` (
  `id_image` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name_img` char(100) DEFAULT NULL,
  PRIMARY KEY (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `image` */

/*Table structure for table `note` */

DROP TABLE IF EXISTS `note`;

CREATE TABLE `note` (
  `id_note` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_note`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `note` */

insert  into `note`(`id_note`,`note`) values 
(1,'En proceso de construcción');

/*Table structure for table `portfolio` */

DROP TABLE IF EXISTS `portfolio`;

CREATE TABLE `portfolio` (
  `id_porfolio` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(50) DEFAULT NULL,
  `location` char(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `img` char(100) DEFAULT NULL,
  KEY `id_porfolio` (`id_porfolio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `portfolio` */

insert  into `portfolio`(`id_porfolio`,`title`,`location`,`description`,`img`) values 
(1,'Proyecto Uno','Xal, Veracruz.','Lorem ipsum dolor sit amet consectetur adipiscing elit senectus nec, pharetra rutrum tempor auctor diam taciti penatibus mollis tristique, ','imagen_1515221727.jpg'),
(2,'funciona','orizaba','ultrices ut commodo accumsan eu penatibus dictum aliquet placerat dis tortor sapien pretium ornare vehicula.','imagen_1515240518.jpg'),
(3,'muestra3','veracruz','Lorem ipsum dolor sit amet consectetur adipiscing elit senectus nec, pharetra rutrum tempor auctor diam taciti penatibus mollis tristique, ','imagen_1516032835.jpg');

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) DEFAULT NULL,
  `name_product` char(50) DEFAULT NULL,
  `description` blob,
  `price` double DEFAULT NULL,
  `img` char(250) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `pdf` char(250) DEFAULT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id_category`),
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id_brand`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`id_producto`,`brand_id`,`name_product`,`description`,`price`,`img`,`category_id`,`pdf`) values 
(16,2,'muestra 11','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',74,'imagen_1123325013.png',9,'reporte de avances sistema MhxSotf.pdf'),
(19,4,'Muestra 7','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',74,'imagen_2217084820.png',10,'reporte de avances sistema MhxSotf.pdf'),
(20,3,'muestra 8','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',5,'imagen_2315301387.jpg',9,'leica_ts02.pdf'),
(21,3,'muestra 1','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',0,'imagen_2315304736.jpg',8,'leica_ts02.pdf'),
(22,4,'muestra 9','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',5,'imagen_2315313760.jpg',9,'leica_ts02.pdf'),
(23,4,'muestra 10','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',0,'imagen_2315334834.jpg',8,'leica_ts02.pdf'),
(24,2,'muestra 2','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',0,'imagen_2315365666.jpg',9,'leica_ts02.pdf'),
(25,2,'muestra 3','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',4,'imagen_2315373224.jpg',10,'leica_ts02.pdf'),
(26,2,'muestra 4','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',74,'imagen_2315375635.jpg',8,'leica_ts02.pdf'),
(27,2,'muestra 5','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',0,'imagen_2315394654.jpg',8,'leica_ts02.pdf'),
(28,2,'muestra 6','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',2,'imagen_2315403735.jpg',8,'leica_ts02.pdf'),
(29,2,'muestra 12','Lorem ipsum dolor sit amet consectetur adipiscing elit conubia neque convallis quam habitasse, eu sollicitudin lobortis pretium parturient fringilla iaculis proin aliquet nulla nunc. Gravida litora augue condimentum aenean, magnis senectus in dictum felis, vitae semper nibh. Tincidunt tortor quis viverra nunc nisl luctus vitae magnis sociis, torquent etiam praesent nam nec ornare sociosqu vestibulum neque ultrices, augue malesuada nisi justo eget tellus suspendisse elementum. Elementum scelerisque parturient risus pulvinar torquent velit turpis condimentum accumsan blandit, litora purus pellentesque etiam sodales ullamcorper malesuada fusce augue imperdiet, dignissim vel cubilia posuere porta nulla egestas dapibus congue.\r\n\r\nAugue massa class ultrices felis fames vehicula nascetur taciti hendrerit, tempor cubilia duis eget vel est auctor. Dictumst ullamcorper urna feugiat fermentum imperdiet venenatis fames duis habitant condimentum, ad accumsan suscipit quam rutrum nisi fringilla vitae morbi, suspendisse sed vehicula inceptos ut maecenas pellentesque hendrerit leo. Sed aptent cras tempor massa maecenas curae eleifend diam nullam, hac dui tristique eget accumsan quam placerat ornare augue, conubia pellentesque commodo laoreet netus cum ultrices velit. Mus varius facilisis quisque enim fringilla vel cubilia felis torquent, id arcu suspendisse consequat vulputate libero in ultricies, nam convallis lobortis aenean suscipit lacinia ante hac.',8,'imagen_2322393024.jpg',9,'leica_ts02.pdf');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `password` char(250) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`admin_id`,`user`,`password`) values 
(4,4,'Admin','$2y$10$nN7yafxH6siKv77lqAH6q.4igMXRosgvSdWdicy5CxO1i6J5knIc.');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
